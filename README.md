# VisualSpeechTrackingHearing

Repository to run statistics for the study 'Decreasing hearing ability does not lead to improved visual speech extraction as revealed in a neural speech tracking paradigm'

For analysing the backward modelling results (i.e. Figure 3 and Figure 4A) please use the R script 'R_analysis.R'

The script to recreate statistical analyses for source data (Figure 2 and Figure 4B) 'FourwardSourceResults.m'
For this latter analysis the FieldTrip Toolbox for MATLAB is required.

