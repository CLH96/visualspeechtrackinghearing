%% FIGURE 2 prep
load('SpectrogramModel.mat')
load('SpectrogramLipModel.mat')
load('SpectrogramLexicalModel.mat')


Nsubj = 67;
load standard_mri_better
load mni_grid_1_cm_2982pnts.mat
mri = ft_convert_units(mri,'m');
template_grid = ft_convert_units(template_grid,'m');
atlas= ft_read_atlas('/mnt/obob/obob_ownft/external/fieldtrip/template/atlas/aal/ROI_MNI_V4.nii');
atlas= ft_convert_units(atlas,'m');

% Get regions of interest
template_grid.coordsys = 'mni';
cfg = [];
cfg.atlas = atlas;
cfg.roi = atlas.tissuelabel([1:90]);
idx_region = reshape(ft_volumelookup(cfg,template_grid),[5780,1]);
inside = find(template_grid.inside == 1);

idx_regions_reduced = idx_region(inside);
label_regions = find(idx_regions_reduced == 1);

virt_chan = label_regions;
neighbours = idx_region; 
%% FIGURE 2 Cluster based permutation test  
cfg = [];
cfg.latency = 1; %
cfg.channel = cellstr(string(virt_chan))' ;
cfg.avgovertime = 'yes';
cfg.parameter = 'individual';
cfg.method = 'montecarlo';
cfg.statistic = 'ft_statfun_depsamplesT';

cfg.alpha = 0.05;
cfg.correctm = 'cluster';
cfg.clusterthreshold = 'nonparametric_individual';
cfg.numrandomization = 10000;
cfg.clusteralpha = 0.05;
cfg.clusterstatistic = 'maxsum';
cfg.tail = 1;
cfg.correcttail = 'prob';
cfg.minnbchan = 3;
% prepare neighbours
elec = [];
elec.elecpos = template_grid.pos(neighbours, :);
elec.label = cellstr(string(virt_chan))';
cfg_neighb.method = 'distance';
cfg_neighb.elec = elec;
cfg_neighb.neighbourdist = 0.011 ;
cfg.neighbours = ft_prepare_neighbours(cfg_neighb);
cfg.design = zeros(2, Nsubj*2);
cfg.design(1,1:2*Nsubj)  = [ones(1,Nsubj) 2*ones(1,Nsubj)];
cfg.design(2,1:2*Nsubj)  = [1:Nsubj 1:Nsubj];

%
cfg.ivar                = 1; % the 1st row in cfg.design contains the independent variable
cfg.uvar                = 2; % the 2nd row in cfg.design contains the subject numberr


stat.Spectrogram.Benefit_AV_Clear = ft_timelockstatistics(cfg,SpectrogramModel.r_AV_nomask_nodistS,SpectrogramModel.r_A_nomask_nodistS);
stat.Spectrogram.Benefit_AV_Multi = ft_timelockstatistics(cfg,SpectrogramModel.r_AV_nomask_distS,  SpectrogramModel.r_A_nomask_distS);

stat.AddedValueLip.AV_Clear         = ft_timelockstatistics(cfg,SpectrogramLipModel.r_AV_nomask_nodistS,SpectrogramModel.r_AV_nomask_nodistS);
stat.AddedValueLip.Benefit_AV_Clear = ft_timelockstatistics(cfg,SpectrogramLipModel.Benfit_NoMaskNoDist,SpectrogramModel.Benfit_NoMaskNoDist);
stat.AddedValueLip.AV_Multi          = ft_timelockstatistics(cfg,SpectrogramLipModel.r_AV_nomask_distS,  SpectrogramModel.r_AV_nomask_distS);
stat.AddedValueLip.Benefit_AV_Multi  = ft_timelockstatistics(cfg,SpectrogramLipModel.Benfit_NoMaskDist,  SpectrogramModel.Benfit_NoMaskDist);

stat.AddedValueLexical.AV_Clear         = ft_timelockstatistics(cfg,SpectrogramLexicalModel.r_AV_nomask_nodistS,SpectrogramModel.r_AV_nomask_nodistS);
stat.AddedValueLexical.Benefit_AV_Clear = ft_timelockstatistics(cfg,SpectrogramLexicalModel.Benfit_NoMaskNoDist,SpectrogramModel.Benfit_NoMaskNoDist);
stat.AddedValueLexical.AV_Multi          = ft_timelockstatistics(cfg,SpectrogramLexicalModel.r_AV_nomask_distS,  SpectrogramModel.r_AV_nomask_distS);
stat.AddedValueLexical.Benefit_AV_Multi  = ft_timelockstatistics(cfg,SpectrogramLexicalModel.Benfit_NoMaskDist,  SpectrogramModel.Benfit_NoMaskDist);
%% FIGURE 4B prep
VisualTrackingData = readtable('VisualTrackingData.csv');
load('AddedValueLip.mat');

Nsubj = 67;
load standard_mri_better
load mni_grid_1_cm_2982pnts.mat
mri = ft_convert_units(mri,'m');
template_grid = ft_convert_units(template_grid,'m');
atlas= ft_read_atlas('/mnt/obob/obob_ownft/external/fieldtrip/template/atlas/aal/ROI_MNI_V4.nii');
atlas= ft_convert_units(atlas,'m');

% Get regions of interest
template_grid.coordsys = 'mni';
cfg = [];
cfg.atlas = atlas;
cfg.roi = atlas.tissuelabel([43:54]); % 43-54 visual cortex
idx_region = reshape(ft_volumelookup(cfg,template_grid),[5780,1]);
inside = find(template_grid.inside == 1);

idx_regions_reduced = idx_region(inside);
label_regions = find(idx_regions_reduced == 1);

virt_chan = label_regions; 
neighbours = idx_region;
%% 4B Cluster-corrected correlations
cfg = [];
cfg.latency = 1; %
cfg.avgovertime = 'yes';
cfg.parameter = 'individual';
cfg.method = 'montecarlo';
cfg.statistic ='ft_statfun_correlationT';
cfg.channel = cellstr(string(virt_chan))' ;


cfg.type = 'Spearman';
cfg.alpha = 0.05;
cfg.correctm = 'cluster';
cfg.clusterthreshold = 'nonparametric_individual';
cfg.numrandomization = 10000;
cfg.clusteralpha = 0.05;

cfg.clusterstatistic = 'maxsum';
cfg.tail = 1;
cfg.correcttail = 'prob';
cfg.minnbchan = 1;
% prepare neighbours
elec = [];
elec.elecpos = template_grid.pos(neighbours, :);
elec.label = cellstr(string(virt_chan))';
cfg_neighb.method = 'distance';
cfg_neighb.elec = elec;
cfg_neighb.neighbourdist = 0.011 ;
cfg.neighbours = ft_prepare_neighbours(cfg_neighb);
cfg.design = VisualTrackingData.VisualTrackingScore;

stat.AV_Clear = ft_timelockstatistics(cfg,AddedValueLip.AV_Clear);
stat.BenefitAV_Clear = ft_timelockstatistics(cfg,AddedValueLip.Benefit_AV_Clear);
stat.AV_Multi = ft_timelockstatistics(cfg,AddedValueLip.AV_Multi);
stat.BenefitAV_Multi = ft_timelockstatistics(cfg,AddedValueLip.Benefit_AV_Multi);